# Wireguard tooling

## Server side
Assumptions:
- vpn network name: wg0

Installation on server side, first generate the keys and then use the template in order to create the configuration:
```bash
sh install-rpi.sh
```

Generate keys:
```bash
sh setup.sh
```

After installation would be useful to have services to control the vpn, use normal `systemctl commands <start | restart | stop | enable | disable>`:
```bash
systemctl enable wg-quick@wg0
systemctl start wg-quick@wg0
```

Or if you want to do it manually:

```bash
wg-quick up wg0
```

## Client Side
In mac and windows, better use the UX to add a new tunnel, this will create the key-pair. Then merge the `peer.conf` configuration.

In Linux, use the setup.sh`script` to generate the keys, copy the `peer.conf` at `/etc/wireguard/tunnel_name.conf` and change the values accordingly.
Then:
```bash
wg-quick up tunnel_name
```

PS: another different but more complex approach: https://github.com/adrianmihalko/wg_config
