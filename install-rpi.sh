#!/usr/bin/env bash

sudo apt update && sudo apt upgrade -y
sudo apt-get install raspberrypi-kernel-headers

echo "deb http://deb.debian.org/debian/ unstable main" | sudo tee --append /etc/apt/sources.list.d/unstable.list
wget -O - https://ftp-master.debian.org/keys/archive-key-$(lsb_release -sr).asc | sudo apt-key add -


# Priority to do not prefer unstable rather than stable packages
printf 'Package: *\nPin: release a=unstable\nPin-Priority: 150\n' | sudo tee --append /etc/apt/preferences.d/limit-unstable
sudo apt update
sudo apt install wireguard qrencode -y
